param (
    [string]$xml,
    [string]$xdt,
    [string]$output
)

# Vérifiez que les fichiers d'entrée existent
if (-not (Test-Path $xml) -or -not (Test-Path $xdt)) {
    Write-Host "Les fichiers d'entrée ($xml, $xdt) n'existent pas."
    exit 1
}

# Vérifiez que le fichier de sortie n'existe pas encore
if (Test-Path $output) {
    Write-Host "Le fichier de sortie ($output) existe déjà."
    #exit 1
}

    $scriptPath = $MyInvocation.MyCommand.Path
    $scriptPath = Split-Path $scriptPath

    $transformDll = ""
    if(Test-Path "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v12.0\Web\Microsoft.Web.XmlTransform.dll"){
        $transformDll = "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v12.0\Web\Microsoft.Web.XmlTransform.dll"
    }
    if(($transformDll -eq "")-and(Test-Path "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v11.0\Web\Microsoft.Web.XmlTransform.dll")){
        $transformDll = "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v11.0\Web\Microsoft.Web.XmlTransform.dll"
    }
    if(($transformDll -eq "")-and(Test-Path "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v10.0\Web\Microsoft.Web.XmlTransform.dll")){
        $transformDll = "C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v10.0\Web\Microsoft.Web.XmlTransform.dll"
    }
    if($transformDll -eq ""){
        $transformDll = "$scriptPath\Microsoft.Web.XmlTransform.dll"
    }

    Add-Type -Path $transformDll

# Exécutez la transformation XML
try {
    Write-Host "Transformation en cours..."
    $xmlDocument = New-Object Microsoft.Web.XmlTransform.XmlTransformableDocument;
    $xmlDocument.PreserveWhitespace = $true
    $xmlDocument.Load($xml);

    $transformation = New-Object Microsoft.Web.XmlTransform.XmlTransformation($xdt)
    $success = $transformation.Apply($xmlDocument)

    if ($success) {
        Write-Host "Transformation réussie."
        $xmlDocument.Save($output)
        Write-Host "Résultat sauvegardé dans $output."
    } else {
        Write-Host "La transformation a échoué."
        exit 1
    }
} catch {
    Write-Host "Une erreur s'est produite lors de la transformation : $($_.Exception.Message)"
    exit 1
}
