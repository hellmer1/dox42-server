﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TemplateControl.ascx.cs" Inherits="dox42Live.TemplateControl" %>


<div id="divTemplateFrame"  class="TemplateControl" style="display:block">
  <div id="divLeft" style="float: left; width:85%; text-align:center;"  >
    <asp:ImageButton ID="TemplateImage" runat="server" ImageAlign="Left" Width="55px" OnClick="btnChooseTemplate_Click"  />
    <asp:LinkButton ID="btnTemplateName" runat="server" CssClass="TemplateName"  OnClick="btnTemplateName_Click"/>
  </div>
  <div id="divButton" style="float: right; width:15%; text-align: right;"  >
    <asp:ImageButton ID="btnBack" runat="server" Width="42px"  ImageUrl="GoBack.gif" OnClick="btnBack_Click" />
    <asp:ImageButton ID="btnChooseTemplate" runat="server" Width="42px" ImageUrl="Go.gif" OnClick="btnChooseTemplate_Click"  />
  </div>
</div>
 