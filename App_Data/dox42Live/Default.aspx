﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="dox42Live.dox42Live"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>dox42 LIVE</title>

    <link rel="stylesheet" type="text/css" href="dox42Live.css" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />

    <script type="text/javascript">
        function onLoad(theKey) {
            if (document.getElementById("txtSearch") != null) {
                var searchString = document.getElementById("txtSearch").value.toLowerCase();
                var elements = document.getElementsByClassName('TemplateName');

                for (var i = 0; i < elements.length; i++) {

                    if (elements[i].innerText.toLowerCase().indexOf(searchString) == -1) {
                        elements[i].parentNode.parentNode.style.display = "none";
                    }
                    else {
                        elements[i].parentNode.parentNode.style.display = "block";
                    }
                }

                return;
            }

            var listGoControls = document.getElementsByClassName("InputParamControl");
            if (listGoControls != null && listGoControls.length > 0) {
                var divLastIP = listGoControls[listGoControls.length - 1];

                divLastIP.scrollIntoView(true);

                var combo = divLastIP.getElementsByClassName("combo");
                if (combo != null && combo.length > 0)
                    combo[0].focus();

                var txtBox = divLastIP.getElementsByClassName("textbox");
                if (txtBox != null && txtBox.length > 0)
                    txtBox[0].focus();
            }

        }

        // Prevent Back Button when Template generating
        function preventBack() {
            if (document.getElementById("txtSearch") != null)
                window.history.forward();
        }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>
</head>

<body onload="return onLoad(event)">
    <form id="MainForm" runat="server" style="position:relative; max-width:640px;   margin:0 auto 0;" >
       
        <div id="divHeader" runat="server" class="divHeader">
            <img alt="dox42 Live" src="dox42LIVE_Logo.jpg" style="margin: 10px; width:220px;" />
            <asp:Label ID="lblTrialMode" runat="server" Text="TRIAL MODE"></asp:Label>
        </div>

        <div id="divSearch" class ="divSearch" runat="server">  
             <asp:TextBox ID="txtSearch" CssClass="textbox"  placeholder="Search"  runat="Server" onkeyup="return onLoad(event)" oninput="return onLoad(event)" ></asp:TextBox>
        </div>

        <asp:PlaceHolder ID="TemplatesPlaceHolder" runat="server"></asp:PlaceHolder>
        <asp:PlaceHolder ID="InputParamPlaceHolder" runat="server"></asp:PlaceHolder>

        <div id="divError" class ="divError" runat="server" >
            <br />
            <asp:Label id="lblMessage"  runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
