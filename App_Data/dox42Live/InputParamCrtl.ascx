﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InputParamCrtl.ascx.cs" Inherits="dox42Live.InputParamCrtl"  %>
<div id="divInputParam" class="InputParamControl" >
    <div id="divIPControls" style="float: left; width:85%" >
    <asp:Label ID="lblInputParamName" runat="server" Text="InputParam" CssClass="InputParamH"></asp:Label>
    </div>
     <div id="divGoControls" class="divGoControls" style="float: right; width:15%; text-align: right;"  >
        <asp:ImageButton ID="btnBack" runat="server"  OnClick="btnBack_Click" Height="16px" ImageUrl="Cancel.gif"/><br>
     </div>

    <asp:DropDownList ID="cmbInputParam" CssClass="combo" runat="server">
    </asp:DropDownList>
    <asp:TextBox ID="txtInputParam" CssClass="textbox" TextMode="MultiLine" runat="Server"></asp:TextBox><br />
    <asp:FileUpload Visible="false" ID="fuInputParam" CssClass="textbox" runat="server" />
     <div id="divComment" style="float: left; width:85%" >
    <asp:Label ID="lblComment" runat="server" Text="Comment" CssClass="Comment"></asp:Label>
   </div> 
        <div id="divButtons" class="divGoControls" style="float: right; width:15%; text-align: right;"  >
    <asp:ImageButton ID="btnGo" runat="server"  OnClick="btnGo_Click" Width="42px" ImageUrl="OK.gif" />

        </div>
</div>
